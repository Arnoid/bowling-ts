import { Bowling } from "../src/main";

describe("Bowling time !", () => {
  it("Should count one throw", () => {
    const bowling = new Bowling();

    bowling.roll(1);

    expect(bowling.score).toEqual(1);
  });

  it("Should count two throws", () => {
    const bowling = new Bowling();

    bowling.roll(1);
    bowling.roll(1);

    expect(bowling.score).toEqual(2);
  });

  it("Should not be able to roll more than 20 times", () => {
    const bowling = new Bowling();

    Array.from({ length: 20 }).forEach(() => bowling.roll(1));

    expect(() => bowling.roll(1)).toThrow(Error);
  });

  it("Should not be able to provide a negative result", () => {
    const bowling = new Bowling();

    expect(() => bowling.roll(-1)).toThrow(Error);
  });

  it("Should not be able to provide a result > 10", () => {
    const bowling = new Bowling();

    expect(() => bowling.roll(11)).toThrow(Error);
  });

  it("Should not be able to make more than 10 pins fail in two rows", () => {
    const bowling = new Bowling();

    bowling.roll(1);

    expect(() => bowling.roll(10)).toThrow(Error);
  });

  it("Should correctly count a spare", () => {
    const bowling = new Bowling();

    bowling.roll(5);
    bowling.roll(5);
    bowling.roll(5);

    expect(bowling.score).toEqual(20);
  });

  it("Should return to turn 1 when there's a strike", () => {
    const bowling = new Bowling();

    bowling.roll(10);

    expect(bowling.turn).toEqual(1);
  });

  it("Should correctly count a strike", () => {
    const bowling = new Bowling();

    bowling.roll(10);
    bowling.roll(1);
    bowling.roll(1);

    expect(bowling.score).toEqual(14);
  });

  it("Should benefit to one more roll if last rolls make a spare", () => {
    const bowling = new Bowling();

    Array.from({ length: 19 }).forEach(() => bowling.roll(1));
    bowling.roll(9);

    expect(() => bowling.roll(1)).not.toThrow(Error);
  });

  it("Should benefit to two more rolls if last roll is a strike", () => {
    const bowling = new Bowling();

    Array.from({ length: 18 }).forEach(() => bowling.roll(1));
    bowling.roll(10);
    bowling.roll(1);

    expect(() => bowling.roll(1)).not.toThrow(Error);
  });

  it("Should not be able to make infinite strikes", () => {
    const bowling = new Bowling();

    Array.from({ length: 12 }).forEach(() => bowling.roll(10));

    expect(() => bowling.roll(10)).toThrow(Error);
  });
});
