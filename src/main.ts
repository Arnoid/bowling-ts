export class Bowling {
  public score: number = 0;
  private pinCount: number = 10;
  public turn: number = 1;
  private isSpareActivated: boolean = false;
  private remainingStrikeActivationCount: number = 0;
  private roundCount: number = 0;

  public roll(result: number): void {
    if (this.roundCount >= 10 && !this.isSpareActivated && this.remainingStrikeActivationCount === 0) {
      throw new Error();
    }
    if (result < 0 || result > 10) {
      throw new Error();
    }
    if (result > this.pinCount) {
      throw new Error();
    }
    this.score +=
      result * (this.isSpareActivated || this.remainingStrikeActivationCount > 0 ? 2 : 1);
    this.pinCount -= result;
    if (this.turn === 2 && this.pinCount === 0) {
      this.isSpareActivated = true;
    }
    if (result === 10 && this.roundCount < 10) {
      this.remainingStrikeActivationCount = 2;
    } else {
      this.remainingStrikeActivationCount = Math.max(0, this.remainingStrikeActivationCount - 1);
    }
    if (this.turn === 1 && result !== 10) {
      this.turn = 2;
    } else {
      this.turn = 1;
      this.roundCount += 1;
      this.pinCount = 10;
    }
  }
}
